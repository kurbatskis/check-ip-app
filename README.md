# Test task

## Setup the project

```
cd checkIpApp/docker

docker-compose up -d
```

## Run the CLI command

```
docker-compose run --rm php-fpm php bin/console app:ip-info --output_format=cli

docker-compose run --rm php-fpm php bin/console app:ip-info --output_format=json
```

## Run the tests
```
docker-compose run --rm php-fpm php ./vendor/bin/phpunit
```