<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Command\IpInfo;
use App\Service\CliTableFactory;
use App\Service\IpInfo as IpInfoService;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\ResponseInterface;

use function trim;

class IpInfoTest extends TestCase
{
    use ProphecyTrait;

    private $ipInfoService;
    private $filesystem;
    private $cliTableFactory;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->ipInfoService = $this->prophesize(IpInfoService::class);
        $this->filesystem = $this->prophesize(Filesystem::class);
        $this->cliTableFactory = $this->prophesize(CliTableFactory::class);
    }

    /**
     * @dataProvider commandExecutionWithWrongOutputFormatOptionDataProvider
     */
    public function testCommandExecutionWithWrongOutputFormatOption($outputFormat): void
    {
        $command = new IpInfo(
            $this->ipInfoService->reveal(),
            $this->filesystem->reveal(),
            $this->cliTableFactory->reveal()
        );

        $commandTester = new CommandTester($command);
        $commandTester->execute(['--output_format' => $outputFormat]);
        self::assertSame('Invalid output format. Allowed options is "cli" or "json"', trim($commandTester->getDisplay()));
        self::assertSame(1, $commandTester->getStatusCode());
    }

    public function commandExecutionWithWrongOutputFormatOptionDataProvider(): array
    {
        return [
            'passed "foo" argument' => [
                'output_format' => 'foo',
            ],
            'passed "bar" argument' => [
                'output_format' => 'bar',
            ],
            'passed "jso" argument' => [
                'output_format' => 'jso',
            ],
            'passed "son" argument' => [
                'output_format' => 'son',
            ],
            'passed "jsonn" argument' => [
                'output_format' => 'jsonn',
            ],
        ];
    }

    public function testCommandExecutionDuringApiRequest(): void
    {
        $response = $this->prophesize(ResponseInterface::class);
        $response->getInfo('http_code')->willReturn(500);
        $response->getInfo('url')->willReturn('http://example.com');
        $response->getInfo('response_headers')->willReturn([]);

        $this->ipInfoService->getIpAddressData()->willThrow(new ClientException($response->reveal()));

        $command = new IpInfo(
            $this->ipInfoService->reveal(),
            $this->filesystem->reveal(),
            $this->cliTableFactory->reveal()
        );

        $commandTester = new CommandTester($command);
        $commandTester->execute(['--output_format' => 'cli']);

        self::assertSame('HTTP 500 returned for "http://example.com".', trim($commandTester->getDisplay()));
        self::assertSame(1, $commandTester->getStatusCode());
    }

    /**
     * @dataProvider commandExecutionWithOutputInCliDataProvider
     */
    public function testCommandExecutionWithOutputInCli(array $dataFromApi, array $expectedResult): void
    {
        self::markTestIncomplete(
            'This test has not been implemented yet.'
        );

        $table = $this->prophesize(Table::class);
        $table->setHeaders(
            [
                'IP',
                'City',
                'Region',
                'Country',
                'Latitude and longitude',
                'Organization',
                'Postal',
                'Timezone',
            ]
        )->shouldBeCalledOnce()->willReturn($table);
        $table->setRows($expectedResult)->shouldBeCalledOnce()->willReturn($table);
        $table->render()->shouldBeCalledOnce();

        $this->ipInfoService->getIpAddressData()->willReturn($dataFromApi);
        $this->cliTableFactory
            ->createTable(Argument::type(OutputInterface::class))
            ->willReturn($table->reveal());

        $command = new IpInfo(
            $this->ipInfoService->reveal(),
            $this->filesystem->reveal(),
            $this->cliTableFactory->reveal()
        );

        $commandTester = new CommandTester($command);
        $commandTester->execute(['--output_format' => 'cli']);

        self::assertSame(0, $commandTester->getStatusCode());
    }

    public function commandExecutionWithOutputInCliDataProvider(): array
    {
        return [
            'all required data is passed from API' => [
                'api data'        => [
                    'ip'       => '195.124.1.127',
                    'city'     => 'Riga',
                    'region'   => 'Riga',
                    'country'  => 'LV',
                    'loc'      => '56.95910162571335, 24.11441127331811',
                    'org'      => 'example',
                    'postal'   => 'LV-1010',
                    'timezone' => 'Europe/Riga',
                ],
                'expected result' => [
                    '195.124.1.127',
                    'Riga',
                    'Riga',
                    'LV',
                    '56.95910162571335, 24.11441127331811',
                    'example',
                    'LV-1010',
                    'Europe/Riga',
                ],
            ],
        ];
    }

    /**
     * @dataProvider commandExecutionWithOutputInJsonFileDataProvider
     */
    public function testCommandExecutionWithOutputInJsonFile(array $dataFromApi, string $expectedResult): void
    {
        $this->ipInfoService->getIpAddressData()->willReturn($dataFromApi);
        $this->filesystem->exists('some/path/ip_info.json')->willReturn(false);
        $this->filesystem->dumpFile('some/path/ip_info.json', $expectedResult)->shouldBeCalledOnce();

        $command = new IpInfo(
            $this->ipInfoService->reveal(),
            $this->filesystem->reveal(),
            $this->cliTableFactory->reveal()
        );

        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--output_format' => 'json',
                '--file_path'     => 'some/path',
            ]
        );

        self::assertSame(0, $commandTester->getStatusCode());
    }

    public function commandExecutionWithOutputInJsonFileDataProvider(): array
    {
        return [
            'all required data is passed from API' => [
                'api data'        => [
                    'ip'       => '195.124.1.127',
                    'city'     => 'Riga',
                    'region'   => 'Riga',
                    'country'  => 'LV',
                    'loc'      => '56.95910162571335, 24.11441127331811',
                    'org'      => 'example',
                    'postal'   => 'LV-1010',
                    'timezone' => 'Europe/Riga',
                ],
                'expected result' => '{"ip":"195.124.1.127","city":"Riga","region":"Riga","country":"LV","loc":"56.95910162571335, 24.11441127331811","org":"example","postal":"LV-1010","timezone":"Europe\/Riga"}',
            ],
            'some api data does not exist'         => [
                'api data'        => [
                    'ip'      => '195.124.1.127',
                    'city'    => 'Riga',
                    'region'  => 'Riga',
                    'country' => 'LV',
                    'org'     => '',
                    'postal'  => 'LV-1010',
                ],
                'expected result' => '{"ip":"195.124.1.127","city":"Riga","region":"Riga","country":"LV","org":"","postal":"LV-1010"}',
            ],
            'empty data from api'                  => [
                'api data'        => [],
                'expected result' => '[]',
            ],
        ];
    }

    public function testCommandExecutionWithOutputInJsonFileIfFileAlreadyExist(): void
    {
        $questionHelper = $this->prophesize(QuestionHelper::class);
        $questionHelper->ask(Argument::any(), Argument::any(), Argument::any())->willReturn(true);

        $helperSet = $this->prophesize(HelperSet::class);
        $helperSet->get('question')->willReturn($questionHelper->reveal());

        $dataFromApi = [
            'ip'       => '195.124.1.127',
            'city'     => 'Riga',
            'region'   => 'Riga',
            'country'  => 'LV',
            'loc'      => '56.95910162571335, 24.11441127331811',
            'org'      => 'example',
            'postal'   => 'LV-1010',
            'timezone' => 'Europe/Riga',
        ];
        $expectedResult = '{"ip":"195.124.1.127","city":"Riga","region":"Riga","country":"LV","loc":"56.95910162571335, 24.11441127331811","org":"example","postal":"LV-1010","timezone":"Europe\/Riga"}';

        $this->ipInfoService->getIpAddressData()->willReturn($dataFromApi);
        $this->filesystem->exists('some/path/ip_info.json')->willReturn(true);
        $this->filesystem->dumpFile('some/path/ip_info.json', $expectedResult)->shouldBeCalledOnce();
        $this->filesystem->chmod('some/path/ip_info.json', 0777)->shouldBeCalledOnce();

        $command = new IpInfo(
            $this->ipInfoService->reveal(),
            $this->filesystem->reveal(),
            $this->cliTableFactory->reveal()
        );
        $command->setHelperSet($helperSet->reveal());

        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--output_format' => 'json',
                '--file_path'     => 'some/path',
            ],
            [
                '--no-interaction',
            ]
        );
        self::assertSame(0, $commandTester->getStatusCode());
    }
}
