<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *  Class CliTableFactory
 *
 * @author  Alexander Kurbatsky
 */
class CliTableFactory
{
    public function createTable(OutputInterface $output): Table
    {
        return new Table($output);
    }
}
