<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\Client\ApiClient;

/**
 *  Class IpInfo
 *
 * @author  Alexander Kurbatsky
 */
class IpInfo
{
    private $apiClient;

    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @return array
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getIpAddressData(): array
    {
        return $this->apiClient->fetchIpInformation();
    }
}
