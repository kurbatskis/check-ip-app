<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\CliTableFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\IpInfo as IpInfoService;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;
use Throwable;

use function in_array;
use function json_encode;
use function sprintf;

/**
 *  Class ipInfo
 *
 * @author Alexander Kurbatsky
 */
class IpInfo extends Command
{
    private const OUTPUT_TYPE_CLI = 'cli';
    private const OUTPUT_TYPE_JSON = 'json';
    private const DEFAULT_JSON_FILE_NAME = 'ip_info.json';
    private const DEFAULT_JSON_FILE_PATH = 'var/files';

    protected static $defaultName = 'app:ip-info';

    private $ipInfoService;
    private $filesystem;
    private $cliTableFactory;

    public function __construct(IpInfoService $ipInfoService, Filesystem $filesystem, CliTableFactory $cliTableFactory)
    {
        parent::__construct();
        $this->ipInfoService = $ipInfoService;
        $this->filesystem = $filesystem;
        $this->cliTableFactory = $cliTableFactory;
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'output_format',
                'o',
                InputOption::VALUE_OPTIONAL,
                'Output format - CLI table or JSON file',
                self::OUTPUT_TYPE_CLI
            )
            ->addOption(
                'file_path',
                'f',
                InputOption::VALUE_OPTIONAL,
                'Path to store JSON output file',
                self::DEFAULT_JSON_FILE_PATH
            )
            ->setDescription('Get information about your IP address')
            ->setHelp('This command allows you to get information about your IP address');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $outputFormat = $input->getOption('output_format');
        if (!in_array($outputFormat, [self::OUTPUT_TYPE_CLI, self::OUTPUT_TYPE_JSON], true)) {
            $output->writeln('<error>Invalid output format. Allowed options is "cli" or "json"</error>');
            return Command::FAILURE;
        }

        try {
            $ipData = $this->ipInfoService->getIpAddressData();
        } catch (Throwable $exception) {
            $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
            return Command::FAILURE;
        }

        if ($outputFormat === self::OUTPUT_TYPE_CLI) {
            $table = $this->cliTableFactory->createTable($output);
            $table
                ->setHeaders(
                    [
                        'IP',
                        'City',
                        'Region',
                        'Country',
                        'Latitude and longitude',
                        'Organization',
                        'Postal',
                        'Timezone',
                    ]
                )
                ->setRows([
                    [
                        $this->displayTableCell($ipData, 'ip'),
                        $this->displayTableCell($ipData, 'city'),
                        $this->displayTableCell($ipData, 'region'),
                        $this->displayTableCell($ipData, 'country'),
                        $this->displayTableCell($ipData, 'loc'),
                        $this->displayTableCell($ipData, 'org'),
                        $this->displayTableCell($ipData, 'postal'),
                        $this->displayTableCell($ipData, 'timezone'),
                    ]
                ]);
            $table->render();
        } else {
            $filePathWithFileName = $input->getOption('file_path') . '/' . self::DEFAULT_JSON_FILE_NAME;
            if ($this->filesystem->exists($filePathWithFileName)) {
                $helper = $this->getHelper('question');
                $question = new ConfirmationQuestion(
                    'JSON file already exists. Are you sure that need to override this file content?',
                    false
                );

                if (!$helper->ask($input, $output, $question)) {
                    $output->writeln('<bg=yellow;options=bold>JSON file has not been modified</>');
                } else {
                    $this->filesystem->dumpFile($filePathWithFileName, json_encode($ipData));
                    $this->filesystem->chmod($filePathWithFileName, 0777);
                }
            } else {
                $this->filesystem->dumpFile($filePathWithFileName, json_encode($ipData));
            }

            $output->writeln(
                sprintf('<info>JSON file was successfully generated and saved into %s</info>', $filePathWithFileName)
            );
        }

        return Command::SUCCESS;
    }

    private function displayTableCell(array $arrayOfIpData, string $arrayKey): string
    {
        return $arrayOfIpData[$arrayKey] ?? 'N/A';
    }
}
